﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemyhp : MonoBehaviour
{
    [SerializeField] private GameObject nextLevelUI;
    [SerializeField] private Text enemyHPUI;
    [SerializeField] private GameObject enemy01;
    [SerializeField] private GameObject player01;
    [SerializeField] private int hpEnemy = 100;
    private int damageHit = 10;
    private int ultimateDamage = 50;
    private int stopTime = 0;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "shoot")
        {
            hpEnemy -= damageHit;
            Debug.Log(hpEnemy);
            enemyHPUI.text = "EnemyHp : " + hpEnemy;
        }
        if (other.gameObject.tag == "ultshoot")
        {
            hpEnemy -= ultimateDamage;
            Debug.Log(hpEnemy);
            enemyHPUI.text = "EnemyHp : " + hpEnemy;
        }
        if(hpEnemy <= 0)
        {
            enemy01.SetActive(false);
            nextLevelUI.SetActive(true);
            Time.timeScale = stopTime;
        }
    }
}
