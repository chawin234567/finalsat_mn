﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gun : MonoBehaviour
{
    [SerializeField] private AudioSource sound;
    [SerializeField] private AudioClip soundShoot;
    [SerializeField] private AudioClip soundUlt;
    [SerializeField] public float rateOfFire;
    [SerializeField] private GameObject singleBullet;
    [SerializeField] private GameObject ultimate;
    [SerializeField] private Transform bullet;
    private float nextFireRate;
    [SerializeField] private float volume;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFireRate)
        {
            nextFireRate = Time.time + rateOfFire;
            Instantiate(singleBullet, bullet.position, bullet.rotation);
            sound.PlayOneShot(soundShoot,volume);
        }
        if (Input.GetButton("Fire2") && Time.time > nextFireRate)
        {
            nextFireRate = Time.time + rateOfFire;
            Instantiate(ultimate, bullet.position, bullet.rotation);
            sound.PlayOneShot(soundUlt, volume);
        }
    }
}
