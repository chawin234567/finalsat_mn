﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Starts : MonoBehaviour
{
    [SerializeField] private GameObject enemy01;
    [SerializeField] private GameObject playerShip01;
    [SerializeField] private GameObject UI;
    [SerializeField] private AudioSource sound;
    [SerializeField] private AudioClip soundShoot;
    [SerializeField] private float volume;
    private float nomalTimeScale = 1;
    // Start is called before the first frame update
    void Start()
    {
        Button button = gameObject.GetComponent<Button>();
        sound.PlayOneShot(soundShoot);
        button.onClick.AddListener(StartGame);
    }
    void StartGame()
    {
        playerShip01.SetActive(true);
        enemy01.SetActive(true);
        UI.SetActive(false);
        Time.timeScale = nomalTimeScale;
    }
}
